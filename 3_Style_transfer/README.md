# 3_Style_transfer
Style transfer for data from different pipelines.
Taken from: https://github.com/zyxElsa/InST

The goal is to start with a content image x and a style image y and give an output artistic image which keeps the content of x and adapts the style of y.
During inference (using `InST.ipynb`) one can then choose more details how the two images influence the output image. 
The training can be done only using one image (during trainign x=y).

Input = One image
Output = Model which can be used to do style transfer

I did not work in this thorougly enough, I only adapted the code for slices of the original image. For the 3D use, adapt `/InST/ldm/data/personalized.py` PersonalizedBase() class as well as the convolution dimension of the generative backbone.
