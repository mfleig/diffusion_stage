# Internship on diffusion models using fMRI data

Find more details in the `technical_details.pdf`.

### 1_3d_diffusion_simple_architecture
Simple architecture for 3D image generation. A simple U-Net was used. It outputs 3D tensors.
This was a test to get to know this kind of architecture and 3D data. The results are quite poor. Use 2_3d_diff_medical for more reliable performance.

### 2_3d_diff_medical
Advanced architecture for 3D image generation using an advanced (aka standard) U-Net.
Taken from: https://github.com/FirasGit/medicaldiffusion/tree/master

### 3_Style_transfer
Style transfer for data from different pipelines.
Taken from: https://github.com/zyxElsa/InST

### 4_CNN_classifier
Classifier to predict the pipeline a .nii file is from. Adapted from https://gitlab.inria.fr/egermani/self_taught_decoding/-/tree/master/
Trained on either 12 pipelines (SPM/FSL pipelines with FWHM=5) or all 24 pipelines.

### 5_Metrics
Different metrics to evaluate generated images (wrt original images). Implemented FID score, PSNR and SSIM

### CLIP.pdf
Slides from presentation at Tahiti 2023 on OpenAI's Clip model.

### technical_details.pdf
Above approaches in more details as well as some results.