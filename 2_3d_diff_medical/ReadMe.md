# 2_3d_diff_medical
Advanced architecture for 3D image generation using an advanced (aka standard) U-Net.
Taken from: https://github.com/FirasGit/medicaldiffusion/tree/master
Their main idea: Treat the 3D as grayscale Videos (aka they were inspired by code which treats this kind of 3D data). That is why some of the parameters are named confusinlgy for our case (e.g. 'frames'). 
Did NOT use the VQ-GAN approach to treat the data beforehand.

- Image size: 56×48×48 (had to rotate them in order to have square images/'frames' at every of the 56 ’timesteps’)
- Cosine beta schedule with timesteps = 300
- l1 loss
- Results either as gif or random slice as jpg (note: when slicing at the bottom/top the image shows random noise as there is not brain)

Run on Igrida using `medical_diff_submit.sh`.
Change parameters in `medical_diff.py` in lines 1202 to 1206.

The Notebook `medical_diffusion.ipynb` is only to play around a little bit/get to know the data.

Needed to be trained for far longer (limitation of 48h with max 376 GB RAM on Igrida). Promissing approach!