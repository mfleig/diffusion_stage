#!/bin/bash
#OAR -l core=1,walltime=12:00:00
#source ~/.bashrc
#conda init bash
#  Python script
main_script=/udd/mfleig/Documents/3d_diffusion.py
# Conda environment
source /srv/tempdd/mfleig/miniconda/etc/profile.d/conda.sh
source /srv/tempdd/mfleig/miniconda/bin/activate
conda activate env 
# Launch
python $main_script ${OAR_JOBID}

conda deactivate
