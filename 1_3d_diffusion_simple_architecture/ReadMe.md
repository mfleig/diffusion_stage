# 1_3d_diffusion_simple_architecture
Simple architecture for 3D image generation. A simple U-Net was used. It outputs 3D tensors.
This was a test to get to know this kind of architecture and 3D data. The results are quite poor. Use 2_3d_diff_medical for more reliable performance.

Input: .nii files from random pipeline 
Output: .png files from random slice of generated image (idea: predicted noise via mean because variance is fixed)

Since the data has to be square, the input dimensions were cut to be 46x46x46. This is not recommended, again this was only a test.

Run on server via `oarsub -S ./3d_diffusion_submit.sh`
Change parameters in `3d_diffusion.py` in lines 248 to 258.

– batch size = 50
– Linear scheduler for discrete beta schedules with T = 300 time steps

Find example of results in `./output/id_2488171`. Note that this model was not trained for long enough. The inference is not very promising.
Compressed the model due to size.