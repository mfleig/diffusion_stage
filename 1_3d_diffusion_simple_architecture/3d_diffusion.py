import torchvision
import matplotlib.pyplot as plt
from torchvision import transforms 
from torch.utils.data import Dataset, DataLoader
import numpy as np
from torchvision.transforms import Compose
import os
from PIL import Image
import nibabel as nib
import torch.nn.functional as F
from torch import nn
import math
from torch.optim import Adam
import sys
import random
import torch

#-------------------------------- functions --------------------------------
def linear_beta_schedule(timesteps, start=0.0001, end=0.02):
    return torch.linspace(start, end, timesteps)

def get_index_from_list(vals, t, x_shape):
    """ 
    Returns a specific index t of a passed list of values vals
    while considering the batch dimension.
    """
    batch_size = t.shape[0]
    out = vals.gather(-1, t.cpu())
    return out.reshape(batch_size, *((1,) * (len(x_shape) - 1))).to(t.device)

def forward_diffusion_sample(x_0, t, device="cpu"):
    """ 
    Takes an image and a timestep as input and 
    returns the noisy version of it
    """
    noise = torch.randn_like(x_0)
    sqrt_alphas_cumprod_t = get_index_from_list(sqrt_alphas_cumprod, t, x_0.shape)
    sqrt_one_minus_alphas_cumprod_t = get_index_from_list(
        sqrt_one_minus_alphas_cumprod, t, x_0.shape
    )
    # mean + variance
    return sqrt_alphas_cumprod_t.to(device) * x_0.to(device) \
    + sqrt_one_minus_alphas_cumprod_t.to(device) * noise.to(device), noise.to(device)

def get_loss(model, x_0, t):
    x_noisy, noise = forward_diffusion_sample(x_0, t, device)
    print('noise.shape', noise.shape)
    noise_pred = model(x_noisy, t)
    print('noise_pred.shape', noise_pred.shape)
    return F.l1_loss(noise, noise_pred)

@torch.no_grad()
def sample_timestep(x, t):
    """
    Calls the model to predict the noise in the image and returns 
    the denoised image. 
    Applies noise to this image, if we are not in the last step yet.
    """
    betas_t = get_index_from_list(betas, t, x.shape)
    sqrt_one_minus_alphas_cumprod_t = get_index_from_list(
        sqrt_one_minus_alphas_cumprod, t, x.shape
    )
    sqrt_recip_alphas_t = get_index_from_list(sqrt_recip_alphas, t, x.shape)
    
    # Call model (current image - noise prediction)
    model_mean = sqrt_recip_alphas_t * (
        x - betas_t * model(x, t) / sqrt_one_minus_alphas_cumprod_t
    )
    posterior_variance_t = get_index_from_list(posterior_variance, t, x.shape)
    
    if t == 0:
        return model_mean
    else:
        noise = torch.randn_like(x)
        return model_mean + torch.sqrt(posterior_variance_t) * noise 

@torch.no_grad()
def sample_plot_image(epoch_temp, step_temp, loss_temp):
    # Sample noise
    img_size = IMG_SIZE
    img = torch.randn((1, 1, img_size, img_size, img_size), device=device)
    plt.figure(figsize=(15,15))
    plt.axis('off')
    num_images = 10
    stepsize = int(T//num_images)

    for i in range(0,T)[::-1]:
        t = torch.full((1,), i, device=device, dtype=torch.long)
        img = sample_timestep(img, t)
        if i % stepsize == 0:
            plt.subplot(1, num_images, i//stepsize+1)
            slice_temp = random.randint(0,img.shape[-1]-1)
            show_tensor_image(img[0,:,:,:,slice_temp].detach().cpu())
    plt.tight_layout()
    plt.savefig(new_dir+'/sample_'+curr_job_id+'_epoch'+str(epoch_temp)+'_step'+str(step_temp)+'_loss'+("{:.3f}".format(loss_temp))+'_slice'+str(slice_temp)+'.jpg', bbox_inches="tight")
    plt.show()   

def show_tensor_image(image):
    reverse_transforms = transforms.Compose([
        transforms.Lambda(lambda t: (t + 1) / 2),
        transforms.Lambda(lambda t: t.permute(1, 2, 0)), # CHW to HWC
        transforms.Lambda(lambda t: t * 255.),
        transforms.Lambda(lambda t: t.numpy().astype(np.uint8)),
        transforms.ToPILImage(),
    ])
    plt.imshow(reverse_transforms(image))

#-------------------------------- classes --------------------------------
class FMRIDataset(Dataset):
    def __init__(self, image_path, dataset_name, img_size, og_folder='/original', transform=False):
        self.name = dataset_name
        self.transform = transform
        self.img_size = img_size
        
        # path to folder of current dataset
        dataset_path = image_path+dataset_name
        # path to text file containing all data ids of current dataset
        dataset_ids_path = image_path+dataset_name+'/'+dataset_name+'_IDS.txt'
        
        # create list with all ids in the temp_dataset_path file
        with open(dataset_ids_path) as file:
            all_ids = sorted([line.rstrip()+'.nii' for line in file])
        
        # create list with all ids that are acutally avaiable
        files_in_og_folder = sorted([file for file in os.listdir(dataset_path+og_folder+'/')])
        
        # Check if all data from all_ids-list is avaiable
        assert(all_ids==files_in_og_folder)
        
        files_with_path = [dataset_path+og_folder+'/'+a for a in all_ids]
        self.data_path = files_with_path
            
    def __len__(self):
        return len(self.data_path)

    def __getitem__(self, idx):
        data_filepath = self.data_path[idx]
        # from Elodie: dataset.py
        data_temp = nib.load(data_filepath).get_fdata().copy()
        #------------------------------------------------------------------ CHANGE THIS ----------
        # data_temp = data_temp[:,4:52]
        #-----------------------------------------------------------------------------------------
        data_temp = np.nan_to_num(data_temp)
        data_temp = torch.tensor(data_temp, dtype=torch.float).view((1), * data_temp.shape)
        size = int(self.img_size) + 6
        return data_temp[:,:,6:size,:]

class Block(nn.Module):
    def __init__(self, in_ch, out_ch, time_emb_dim, up=False):
        super().__init__()
        self.time_mlp =  nn.Linear(time_emb_dim, out_ch)
        if up:
            self.conv1 = nn.Conv3d(2*in_ch, out_ch, 3, padding=1)
            self.transform = nn.ConvTranspose3d(out_ch, out_ch, 4, 2, 1)
        else:
            self.conv1 = nn.Conv3d(in_ch, out_ch, 3, padding=1)
            self.transform = nn.Conv3d(out_ch, out_ch, 4, 2, 1)
        self.conv2 = nn.Conv3d(out_ch, out_ch, 3, padding=1)
        self.bnorm1 = nn.BatchNorm3d(out_ch)
        self.bnorm2 = nn.BatchNorm3d(out_ch)
        self.relu  = nn.ReLU()
        
    def forward(self, x, t, ):
        # First Conv
        h = self.bnorm1(self.relu(self.conv1(x)))
        # Time embedding
        time_emb = self.relu(self.time_mlp(t))
        # Extend last 3 dimensions
        time_emb = time_emb[(..., ) + (None, ) * 3]
        # Add time channel
        h = h + time_emb
        # Second Conv
        h = self.bnorm2(self.relu(self.conv2(h)))
        # Down or Upsample
        return self.transform(h)

class SinusoidalPositionEmbeddings(nn.Module):
    def __init__(self, dim):
        super().__init__()
        self.dim = dim

    def forward(self, time):
        device = time.device
        half_dim = self.dim // 2
        embeddings = math.log(10000) / (half_dim - 1)
        embeddings = torch.exp(torch.arange(half_dim, device=device) * -embeddings)
        embeddings = time[:, None] * embeddings[None, :]
        embeddings = torch.cat((embeddings.sin(), embeddings.cos()), dim=-1)
        # TODO: Double check the ordering here
        return embeddings

class SimpleUnet(nn.Module):
    """
    A simplified variant of the Unet architecture.
    """
    def __init__(self):
        super().__init__()
        image_channels = 1
        down_channels = (64, 128, 256, 512, 1024)
        up_channels = (1024, 512, 256, 128, 64)
        out_dim = 1 
        time_emb_dim = 32
        
        # Time embedding
        self.time_mlp = nn.Sequential(
                SinusoidalPositionEmbeddings(time_emb_dim),
                nn.Linear(time_emb_dim, time_emb_dim),
                nn.ReLU()
            )
        
        # Initial projection
        self.conv0 = nn.Conv3d(image_channels, down_channels[0], 3, padding=1)

        # Downsample
        self.downs = nn.ModuleList([])
        self.ups = nn.ModuleList([])
    
            
        for i in range(len(down_channels)-1):
            self.downs.append(Block(down_channels[i], down_channels[i+1],time_emb_dim))
                
        for i in range(len(up_channels)-1):
            self.ups.append(Block(up_channels[i], up_channels[i+1],time_emb_dim, up=True))
    
        self.output = nn.Conv3d(up_channels[-1], 1, out_dim)

    def forward(self, x, timestep):
        # Embedd time
        t = self.time_mlp(timestep)
        # Initial conv
        
        x = self.conv0(x)
        # Unet
        residual_inputs = []
        for down in self.downs:
            x = down(x, t)
            residual_inputs.append(x)

        for up in self.ups:
            residual_x = residual_inputs.pop()
            # Add residual x as additional channels
            x = torch.cat((x, residual_x), dim=1)           
            x = up(x, t)

        return self.output(x)

#-------------------------------- parameters --------------------------------

IMG_SIZE = 48
BATCH_SIZE = 5

path_to_nii = './group_level/group_50/'
dataset_temp = 'DATASET_SOFT_SPM_FWHM_8_MC_PARAM_24_HRF_1'
folder_og = '/orginal'

# Define beta schedule
T = 200
epochs = 100 
#----------------------------------------------------------------------------

curr_job_id = sys.argv[1]
print('current job id:', curr_job_id)

new_dir = './output/id_'+curr_job_id
os.makedirs(new_dir, exist_ok=True)

f = open(new_dir+'/protocol_'+curr_job_id+'.txt',"w+")
f.write('Current job id: ' + curr_job_id + '\n')
f.close()

betas = linear_beta_schedule(timesteps=T)
# Pre-calculate different terms for closed form
alphas = 1. - betas
alphas_cumprod = torch.cumprod(alphas, axis=0)
alphas_cumprod_prev = F.pad(alphas_cumprod[:-1], (1, 0), value=1.0)
sqrt_recip_alphas = torch.sqrt(1.0 / alphas)
sqrt_alphas_cumprod = torch.sqrt(alphas_cumprod)
sqrt_one_minus_alphas_cumprod = torch.sqrt(1. - alphas_cumprod)
posterior_variance = betas * (1. - alphas_cumprod_prev) / (1. - alphas_cumprod)


dataset_custom = FMRIDataset(path_to_nii, dataset_temp, img_size = IMG_SIZE)
dataloader = DataLoader(dataset_custom, batch_size = BATCH_SIZE, shuffle = True)

model = SimpleUnet()
f = open(new_dir+'/protocol_'+curr_job_id+'.txt', "a+")
f.write("Num params: " + str(sum(p.numel() for p in model.parameters())) + '\n')
f.close()

device = "cuda" if torch.cuda.is_available() else "cpu"
model.to(device)
optimizer = Adam(model.parameters(), lr=0.001)

# Simulate forward diffusion
image_temp = next(iter(dataloader))
f = open(new_dir+'/protocol_'+curr_job_id+'.txt', "a+")
f.write('image_temp.shape: ' + str(image_temp.shape) + '\n')
f.close()

plt.figure(figsize=(15,5))
plt.axis('off')
num_images = 10
stepsize = int(T/num_images)
for idx in range(0, T, stepsize):
    t = torch.Tensor([idx]).type(torch.int64)
    plt.subplot(1, num_images+1, (idx//stepsize) + 1)
    image_temp, noise = forward_diffusion_sample(image_temp, t)
    show_tensor_image(image_temp[0,:,:,:,20])
plt.savefig(new_dir+'/forward_test_'+curr_job_id+'.png', bbox_inches="tight")

f = open(new_dir+'/protocol_'+curr_job_id+'.txt', "a+")
f.write('Start training/sampling' + '\n')
f.close()
for epoch in range(epochs):
    for step, batch in enumerate(dataloader):
        optimizer.zero_grad()
        print('batch.shape', batch.shape)
        t = torch.randint(0, T, (BATCH_SIZE,), device=device).long()
        loss = get_loss(model, batch, t)
        f = open(new_dir+'/protocol_'+curr_job_id+'.txt', "a+")
        f.write('loss for epoch ' + str(epoch) + ' and step ' + str(step) + ': ' + str(loss) + '\n')
        f.close()
        loss.backward()
        optimizer.step()
        print(f"Epoch {epoch} | step {step:03d} Loss: {loss.item()} ")
        if step%5 == 0:
            sample_plot_image(epoch, step, loss.item())

print('finished')