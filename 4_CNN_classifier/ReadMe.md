# 4_CNN_classifier
Trained models:
    - 12 Pipelines trained on data with FWHM=5: `/4_CNN_classifier/self_taught_decoding/data/own_data/all_nii_files/preprocessed/pipeline_half_maps_classification_pipeline_model_cnn_4layers_valid_valid_retrain_all_frozen_0_transfered_0_original_epochs_100_batch_size_64_lr_1e-04/`
    - 24 Pipelines trained all data: `/4_CNN_classifier/self_taught_decoding/data/own_data/all_nii_files/preprocessed/pipeline_maps_classification_pipeline_model_cnn_4layers_valid_valid_retrain_all_frozen_0_transfered_0_original_epochs_100_batch_size_64_lr_1e-04/`

Find informations from training (data shape, training loss/accuracy,...) in `12_pipelines.stout` and `24_pipelines.stout`

- Kept original data structure from Elodies data. In this case one would put `get_data.sh` in the `group_50` folder
- Copy `get_data.sh` is the folder with the data from different pipeline. This could look like to following:
```
    ├── group_50
    |   ├── DATASET_SOFT_FSL_FWHM_5_MC_PARAM_0_HRF_0
    |       ├── DATASET_SOFT_FSL_FWHM_5_MC_PARAM_0_HRF_0_IDS.txt
    |       ├── original
    |           ├── n_1_contrast_cue.nii
    |           ├── n_1_contrast_lf.nii
    |           └── ...
    |   ├── DATASET_SOFT_FSL_FWHM_5_MC_PARAM_0_HRF_1
    |       ├── DATASET_SOFT_FSL_FWHM_5_MC_PARAM_0_HRF_1_IDS.txt
    |       ├── original
    |           ├── n_1_contrast_cue.nii
    |           ├── n_1_contrast_lf.nii
    |           └── ...
    |   └── ...
    └── ...
```
- Run via `bash get_data.sh`. This will take some time. The resulting files will be in `~/self_taugt_decoding/data/own_data/all_nii_files/original/`.

- Prepocess data using `preprocess_submit.sh` based on https://gitlab.inria.fr/egermani/pipeline_transition/-/blob/main/src/lib/utils/preprocessing.py The resulting files will be in `~/self_taugt_decoding/data/own_data/all_nii_files/preprocessed/original/`.

- Folder structure will look like this:
```
    ├── group_50
    ├── self_taugt_decoding
    |   ├── 12_pipelines.stdout
    |   ├── 24_pipelines.stdout
    |   ├── cnn_trainer_submit_half.sh
    |   ├── cnn_trainer_submit_pipelines.sh
    |   ├── data
    |       ├── derived
    |           └── ... (model_final.pt)
    |       ├── own_data
    |           ├── all_nii_files
    |               ├── original 
    |               └── preprocessed
    |                   ├──pipeline_half_...
    |                      └── model_final_fold_0.pt
    |                   ├──pipeline_...
    |                      └── model_final_fold_0.pt
    |                   ├── pipeline_half_labels.csv
    |                   ├── pipelines_labels.csv
    |                   ├── test_valid_pipeline_fold_0.txt
    |                   └── ...
    |   ├── install_environment.sh
    |   ├── README.md
    |   ├── results
    |   ├── src
    |   └── ...
    └── ...
```

In the `all_nii_files` folder will be two types of data:
    - The .csv files holing the labels/paths and further info for the classification
    - The .txt files containing the subjects and contrast to be in the train/test/validation splits. The splits for pipepline and pipeline_half are the same.
        --> Note: `test_valid_pipeline_half_t_fold_0.txt` additionally holds the contrast t/tongue.

- Code is from Elodie, only changed the way data was handled.
- Same conda environment, see https://gitlab.inria.fr/egermani/self_taught_decoding/-/blob/master/install_environment.sh

- Test data using `test_classifier.ipynb`

In case you want to be 100% sure about the files/labels extracted uncommend lines 102 to 106 in `/diffusion_stage/4_CNN_classifier/self_taught_decoding/src/lib/datasets.py`
Try more epochs for better results.